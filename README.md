# SQL Fun 2

#### Description
REPORT: A Client forgot his `Password`... again. Could you get it for me? He has a `users` account and his `Lname` is Miller if that helps at all<br/>Oh! and Ken was saying something about a new table called `passwd`; said it was better to separate things

#### Flag
flag{W1ll_Y0u_J01N_M3?}

#### Hints
Try JOINing another table.
It's not an injection challenge, just a regular sql terminal

#### Solution
Query: SELECT * FROM users JOIN passwd ON passwd.user_id = users.id WHERE Lname="Miller"
once the password is found, use base64 to decode it to a flag
